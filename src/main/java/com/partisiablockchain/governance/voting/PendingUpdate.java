package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** A pending update to be applied depending on the result of the poll. */
@Immutable
public final class PendingUpdate implements StateSerializable, DataStreamSerializable {

  private final PollUpdateType pollUpdateType;
  private final LargeByteArray updateRpc;
  private final Hash identifier;

  @SuppressWarnings("unused")
  PendingUpdate() {
    this.pollUpdateType = null;
    this.updateRpc = null;
    this.identifier = null;
  }

  PendingUpdate(PollUpdateType pollUpdateType, LargeByteArray updateRpc, Hash identifier) {
    this.pollUpdateType = pollUpdateType;
    this.updateRpc = updateRpc;
    this.identifier = identifier;
  }

  static PendingUpdate create(PollUpdateType pollUpdateType, byte[] updateRpc, Hash identifier) {
    return new PendingUpdate(pollUpdateType, new LargeByteArray(updateRpc), identifier);
  }

  PollUpdateType.UpdateExecutor prepare() {
    return SafeDataInputStream.readFully(updateRpc.getData(), pollUpdateType::prepare);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeEnum(pollUpdateType);
    stream.writeDynamicBytes(updateRpc.getData());
    identifier.write(stream);
  }

  static PendingUpdate createFromStateAccessor(StateAccessor oldState) {
    Enum<?> enumValue = oldState.get("pollUpdateType").cast(Enum.class);
    PollUpdateType pollUpdateType = PollUpdateType.values()[enumValue.ordinal()];
    LargeByteArray updateRpc = oldState.get("updateRpc").typedValue(LargeByteArray.class);
    Hash identifier = oldState.get("identifier").hashValue();
    return new PendingUpdate(pollUpdateType, updateRpc, identifier);
  }
}
