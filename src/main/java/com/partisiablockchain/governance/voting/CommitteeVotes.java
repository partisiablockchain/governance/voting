package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;

/**
 * A class containing the active block producer committee and their votes. It gets updated by the
 * BlockProducerOrchestration contract.
 */
@Immutable
public record CommitteeVotes(AvlTree<BlockchainAddress, Long> activeCommitteeVotes)
    implements StateSerializable {

  static CommitteeVotes create(List<CommitteeVote> committeeVotes) {
    AvlTree<BlockchainAddress, Long> committee = AvlTree.create();
    for (CommitteeVote cv : committeeVotes) {
      committee = committee.set(cv.voter(), cv.weight());
    }
    return new CommitteeVotes(committee);
  }

  static CommitteeVotes initial() {
    return new CommitteeVotes(AvlTree.create());
  }

  boolean isPartOfCommittee(BlockchainAddress voter) {
    return activeCommitteeVotes.containsKey(voter);
  }

  /**
   * Get count of committee votes.
   *
   * @return count of committee votes
   */
  public int size() {
    return activeCommitteeVotes.size();
  }

  static CommitteeVotes createFromStateAccessor(StateAccessor oldState) {
    AvlTree<BlockchainAddress, Long> activeCommitteeVotes =
        oldState.get("activeCommitteeVotes").typedAvlTree(BlockchainAddress.class, Long.class);
    return new CommitteeVotes(activeCommitteeVotes);
  }
}
