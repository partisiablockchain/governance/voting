package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.DataStreamSerializable;
import java.util.List;

/** A system contract that facilitates voting. */
@AutoSysContract(VotingState.class)
public final class VotingContract {

  static final long POLL_DURATION_MILLIS = 7 * 1000 * 60 * 60 * 24; // 7 days
  static final long REQUIRED_STAKED_TOKENS_FOR_UPDATE_PROPOSAL = 100_000_0000;

  static final class Invocations {

    static final int PROPOSE_UPDATE = 1;
    static final int VOTE = 2;
    static final int CHECK_UPDATE = 3;
    static final int REMOVE_POLL = 4;
    static final int RECEIVE_COMMITTEE_INFORMATION = 5;

    private Invocations() {}
  }

  /**
   * Initialize the voting contract.
   *
   * @param bpOrchestrationContract bp orchestration contract
   * @return initial state
   */
  @Init
  public VotingState create(BlockchainAddress bpOrchestrationContract) {
    return VotingState.initial(bpOrchestrationContract);
  }

  /**
   * Migrate an existing contract to this version.
   *
   * @param oldState accessor for the old state
   * @return the migrated state
   */
  @Upgrade
  public VotingState upgrade(StateAccessor oldState) {

    BlockchainAddress bpOrchestrationContract =
        oldState.get("bpOrchestrationContract").blockchainAddressValue();

    AvlTree<Hash, Poll> polls =
        StateAccessorUtil.toAvlTree(
            oldState.get("polls"), Poll::createFromStateAccessor, Hash.class);

    CommitteeVotes activeCommitteeVotes =
        CommitteeVotes.createFromStateAccessor(oldState.get("activeCommitteeVotes"));

    return new VotingState(bpOrchestrationContract, polls, activeCommitteeVotes);
  }

  /**
   * Proposes an update.
   *
   * <p>The update RPC must match the format specified by the {@link PollUpdateType}
   *
   * <p>This invocation will fail if the invoker previously proposed an update which is still
   * ongoing.
   *
   * <p>This invocation does not immediately start a new poll, but queries the account plugin to
   * check if the proposer has enough tokens staked to start a poll. Polls are started in {@link
   * #proposeUpdateCallback}.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param description the description of the poll
   * @param pollUpdateType the poll update type
   * @param updateRpc the update RPC
   * @return updated state
   */
  @Action(Invocations.PROPOSE_UPDATE)
  public VotingState proposeUpdate(
      SysContractContext context,
      VotingState state,
      String description,
      PollUpdateType pollUpdateType,
      byte[] updateRpc) {
    BlockchainAddress owner = context.getFrom();
    boolean hasPollFromAccount = state.hasPollFromAccount(owner);
    ensure(!hasPollFromAccount, "Each account can have a maximum of 1 pending proposals");
    Hash pollId = determinePollId(owner, pollUpdateType.ordinal(), updateRpc);

    PendingUpdate pendingUpdate =
        PendingUpdate.create(pollUpdateType, updateRpc, context.getCurrentTransactionHash());

    // Ensure that the rpc is valid by checking that the update can be prepared
    pendingUpdate.prepare();

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallbackWithCostFromRemaining(
        Callbacks.proposeUpdate(pollId, owner, description, pendingUpdate));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(context.getFrom(), AccountPluginRpc.getStakedTokens()));

    return state;
  }

  /**
   * Vote on a poll.
   *
   * <p>Only producers part of the current committee can vote.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param pollId the ID of the poll
   * @param vote the vote
   * @return updated state
   */
  @Action(Invocations.VOTE)
  public VotingState vote(
      SysContractContext context, VotingState state, Hash pollId, boolean vote) {
    BlockchainAddress voter = context.getFrom();
    ensure(state.isPartOfCommittee(voter), "Only block producers can vote for updates");
    long voteTime = context.getBlockProductionTime();
    return state.addVote(pollId, voter, vote, voteTime);
  }

  /**
   * Check if the state of an update can be updated.
   *
   * <p>The state of a poll is one of either:
   *
   * <ul>
   *   <li>NO_RESULT, indicating that not enough votes have been received yet,
   *   <li>NO, indicating that enough producers voted no and so the proposed update will not be
   *       applied and the poll will be removed, or
   *   <li>YES, indicating that enough producers votes yes and so the proposed update will be
   *       applied.
   * </ul>
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param pollId the ID of the poll
   * @return updated state
   */
  @Action(Invocations.CHECK_UPDATE)
  public VotingState checkUpdate(SysContractContext context, VotingState state, Hash pollId) {
    Poll poll = state.getPoll(pollId);
    ensure(poll != null, "No poll exists for the given poll id");
    return state.checkUpdate(pollId, context);
  }

  /**
   * Remove a poll.
   *
   * <p>This invocation is called to remove the poll. Can only be called by the poll owner.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param pollId ID of the poll
   * @return updated state
   */
  @Action(Invocations.REMOVE_POLL)
  public VotingState removePoll(SysContractContext context, VotingState state, Hash pollId) {
    Poll poll = state.getPoll(pollId);
    ensure(poll != null, "No poll exists for the given poll id");
    ensure(poll.getOwner().equals(context.getFrom()), "Only the owner can remove a poll");
    return state.removePoll(pollId);
  }

  /**
   * Updates the committee votes.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param activeCommitteeVotes the active committee
   * @return updated state
   */
  @Action(Invocations.RECEIVE_COMMITTEE_INFORMATION)
  public VotingState receiveCommitteeInformation(
      SysContractContext context, VotingState state, List<CommitteeVote> activeCommitteeVotes) {
    CommitteeVotes committeeVotes = CommitteeVotes.create(activeCommitteeVotes);
    ensure(
        context.getFrom().equals(state.getBpOrchestrationContract()),
        "Only the BP orchestration contract can send committee information");
    return state.updateActiveCommitteeVotes(committeeVotes);
  }

  static final class Callbacks {

    static final int PROPOSE_UPDATE = 0;

    private Callbacks() {}

    static DataStreamSerializable proposeUpdate(
        Hash pollId, BlockchainAddress owner, String description, PendingUpdate pendingUpdate) {
      return stream -> {
        stream.writeByte(Callbacks.PROPOSE_UPDATE);
        pollId.write(stream);
        owner.write(stream);
        stream.writeString(description);
        pendingUpdate.write(stream);
      };
    }
  }

  /**
   * Callback executed as part of {@link #proposeUpdate}.
   *
   * <p>If the callback was successful, this method will receive the current amount of staked tokens
   * by a proposer of a poll. If this amount exceeds {@link
   * #REQUIRED_STAKED_TOKENS_FOR_UPDATE_PROPOSAL}, then a new poll is created. Otherwise, an error
   * is raised.
   *
   * <p>This method does nothing if the callback failed.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param callbackContext information about callbacks
   * @param pollId the ID of the poll
   * @param owner the owner of the poll
   * @param description the description of the poll
   * @param pendingUpdate update information
   * @return updated state
   */
  @Callback(Callbacks.PROPOSE_UPDATE)
  public VotingState proposeUpdateCallback(
      SysContractContext context,
      VotingState state,
      CallbackContext callbackContext,
      Hash pollId,
      BlockchainAddress owner,
      String description,
      PendingUpdateRpc pendingUpdate) {
    PendingUpdate update = pendingUpdate.convert();
    long blockProductionTime = context.getBlockProductionTime();
    if (callbackContext.isSuccess()) {
      long stakedTokens = callbackContext.results().get(0).returnValue().readLong();
      ensure(
          stakedTokens >= REQUIRED_STAKED_TOKENS_FOR_UPDATE_PROPOSAL,
          "Too few staked tokens for update proposal");
      return state.createPoll(
          pollId, owner, description, blockProductionTime + POLL_DURATION_MILLIS, update);
    } else {
      return state;
    }
  }

  private static void ensure(boolean pred, String errorString) {
    if (!pred) {
      throw new RuntimeException(errorString);
    }
  }

  static Hash determinePollId(BlockchainAddress proposer, int pollUpdateType, byte[] updateRpc) {
    return Hash.create(
        s -> {
          proposer.write(s);
          s.writeByte(pollUpdateType);
          s.write(updateRpc);
        });
  }
}
