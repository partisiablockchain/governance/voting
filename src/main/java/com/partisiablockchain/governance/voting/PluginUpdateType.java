package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;

/** The type of plugins that can be updated. */
public enum PluginUpdateType {
  /** Account. */
  ACCOUNT {
    @Override
    public void updatePlugin(SysContractContext context, byte[] jar, byte[] updateRpc) {
      context.getInvocationCreator().updateAccountPlugin(jar, updateRpc);
    }

    @Override
    public void updateGlobalPluginState(
        SysContractContext context, GlobalPluginStateUpdate globalPluginStateUpdate) {
      context.getInvocationCreator().updateGlobalAccountPluginState(globalPluginStateUpdate);
    }
  },
  /** Consensus. */
  CONSENSUS {
    @Override
    public void updatePlugin(SysContractContext context, byte[] jar, byte[] updateRpc) {
      context.getInvocationCreator().updateConsensusPlugin(jar, updateRpc);
    }

    @Override
    public void updateGlobalPluginState(
        SysContractContext context, GlobalPluginStateUpdate globalPluginStateUpdate) {
      context.getInvocationCreator().updateGlobalConsensusPluginState(globalPluginStateUpdate);
    }
  },
  /** Routing. */
  ROUTING {
    @Override
    public void updatePlugin(SysContractContext context, byte[] jar, byte[] updateRpc) {
      context.getInvocationCreator().updateRoutingPlugin(jar, updateRpc);
    }

    @Override
    public void updateGlobalPluginState(
        SysContractContext context, GlobalPluginStateUpdate globalPluginStateUpdate) {
      throw new IllegalArgumentException(
          "The global plugin state of a routing plugin cannot be updated");
    }
  },
  /** Shared object store. */
  SHARED_OBJECT_STORE {
    @Override
    public void updatePlugin(SysContractContext context, byte[] jar, byte[] updateRpc) {
      context.getInvocationCreator().updateSharedObjectStorePlugin(jar, updateRpc);
    }

    @Override
    public void updateGlobalPluginState(
        SysContractContext context, GlobalPluginStateUpdate globalPluginStateUpdate) {
      context
          .getInvocationCreator()
          .updateGlobalSharedObjectStorePluginState(globalPluginStateUpdate);
    }
  };

  /**
   * Update plugin.
   *
   * @param context system contract context
   * @param jar plugin jar
   * @param updateRpc update invocation
   */
  public abstract void updatePlugin(SysContractContext context, byte[] jar, byte[] updateRpc);

  /**
   * Update global plugin state.
   *
   * @param context system contract context,
   * @param globalPluginStateUpdate update to state
   */
  public abstract void updateGlobalPluginState(
      SysContractContext context, GlobalPluginStateUpdate globalPluginStateUpdate);
}
