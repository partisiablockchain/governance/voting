package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;

/** A poll that can be voted on to determine if the pending update should be applied. */
@Immutable
public final class Poll implements StateSerializable {

  private final BlockchainAddress owner;
  private final String description;
  private final AvlTree<BlockchainAddress, Vote> votes;
  private final VotingResult result;
  private final long timeoutTimestamp;
  private final PendingUpdate pendingUpdate;

  @SuppressWarnings("unused")
  Poll() {
    this.result = null;
    this.owner = null;
    this.description = null;
    this.votes = null;
    this.timeoutTimestamp = 0;
    this.pendingUpdate = null;
  }

  private Poll(
      BlockchainAddress owner,
      String description,
      AvlTree<BlockchainAddress, Vote> votes,
      VotingResult result,
      long timeoutTimestamp,
      PendingUpdate pendingUpdate) {
    this.result = result;
    this.owner = owner;
    this.description = description;
    this.votes = votes;
    this.timeoutTimestamp = timeoutTimestamp;
    this.pendingUpdate = pendingUpdate;
  }

  VotingResult getResult() {
    return result;
  }

  private VotingResult determineResult(
      AvlTree<BlockchainAddress, Vote> votes, CommitteeVotes committeeVotes) {
    long yesVotes = 0;
    long noVotes = 0;
    long weightedYesAmount = 0;
    long weightedNoAmount = 0;
    long totalUnusedWeight = 0;

    AvlTree<BlockchainAddress, Long> committeeVotesMap = committeeVotes.activeCommitteeVotes();
    for (BlockchainAddress committeeAddress : committeeVotesMap.keySet()) {
      long weightVotes = committeeVotesMap.getValue(committeeAddress);
      if (votes.containsKey(committeeAddress)) {
        Vote vote = votes.getValue(committeeAddress);
        if (vote.getAnswer()) {
          yesVotes += 1;
          weightedYesAmount += weightVotes;
        } else {
          noVotes += 1;
          weightedNoAmount += weightVotes;
        }
      } else {
        totalUnusedWeight += weightVotes;
      }
    }

    long committeeSize = committeeVotes.size();
    boolean hasMoreThanOneThirdVotedNo = noVotes > (double) committeeSize / 3;
    boolean canStillBeWeightedYes = weightedYesAmount + totalUnusedWeight > weightedNoAmount;
    if (hasMoreThanOneThirdVotedNo || !canStillBeWeightedYes) {
      return VotingResult.NO;
    }

    boolean hasTwoThirdOrMoreVotedYes = yesVotes >= (double) committeeSize / 3 * 2;
    long totalWeight = weightedNoAmount + weightedYesAmount;
    boolean moreThanHalfWeightedYes = weightedYesAmount > totalWeight / 2;
    if (hasTwoThirdOrMoreVotedYes && moreThanHalfWeightedYes) {
      return VotingResult.YES;
    } else {
      return VotingResult.NO_RESULT;
    }
  }

  Poll addVote(
      BlockchainAddress voterAddress, boolean voterVote, CommitteeVotes activeCommitteeVotes) {
    Vote vote = Vote.create(voterVote);
    AvlTree<BlockchainAddress, Vote> updatedVotes = votes.set(voterAddress, vote);
    VotingResult result = determineResult(updatedVotes, activeCommitteeVotes);
    return new Poll(owner, description, updatedVotes, result, timeoutTimestamp, pendingUpdate);
  }

  String getDescription() {
    return description;
  }

  long getTimeoutTimestamp() {
    return timeoutTimestamp;
  }

  static Poll initial(
      BlockchainAddress owner,
      String description,
      long timeoutTimestamp,
      PendingUpdate pendingUpdate) {
    return new Poll(
        owner,
        description,
        AvlTree.create(),
        VotingResult.NO_RESULT,
        timeoutTimestamp,
        pendingUpdate);
  }

  PendingUpdate getUpdate() {
    return pendingUpdate;
  }

  /**
   * Create hash of the pending update including the address of the proposer.
   *
   * @return hash of the update and proposer
   */
  public Hash createUpdateHash() {
    return Hash.create(
        h -> {
          owner.write(h);
          pendingUpdate.write(h);
        });
  }

  BlockchainAddress getOwner() {
    return owner;
  }

  /**
   * Check if poll has a vote from an account.
   *
   * @param account address of the account
   * @return true if account has voted, otherwise false
   */
  public boolean hasVoteFromAccount(BlockchainAddress account) {
    return votes.containsKey(account);
  }

  /**
   * Check if poll is expired.
   *
   * @param voteTime timestamp of the vote
   * @return true if the poll has expired, else false
   */
  public boolean isExpired(long voteTime) {
    return timeoutTimestamp < voteTime;
  }

  static Poll createFromStateAccessor(StateAccessor oldState) {
    BlockchainAddress owner = oldState.get("owner").blockchainAddressValue();
    String description = oldState.get("description").stringValue();
    AvlTree<BlockchainAddress, Vote> votes =
        StateAccessorUtil.toAvlTree(
            oldState.get("votes"),
            (accessor) -> Vote.create(accessor.get("answer").booleanValue()),
            BlockchainAddress.class);
    Enum<?> enumValue = oldState.get("result").cast(Enum.class);
    VotingResult result = VotingResult.values()[enumValue.ordinal()];
    long timeoutTimestamp = oldState.get("timeoutTimestamp").longValue();
    PendingUpdate pendingUpdate =
        PendingUpdate.createFromStateAccessor(oldState.get("pendingUpdate"));

    return new Poll(owner, description, votes, result, timeoutTimestamp, pendingUpdate);
  }
}
