package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;

/** State for the {@link VotingContract}. */
@Immutable
public final class VotingState implements StateSerializable {

  private final BlockchainAddress bpOrchestrationContract;
  private final AvlTree<Hash, Poll> polls;
  private final CommitteeVotes activeCommitteeVotes;

  @SuppressWarnings("unused")
  VotingState() {
    this.bpOrchestrationContract = null;
    this.polls = null;
    this.activeCommitteeVotes = null;
  }

  VotingState(
      BlockchainAddress bpOrchestrationContract,
      AvlTree<Hash, Poll> polls,
      CommitteeVotes activeCommitteeVotes) {
    this.bpOrchestrationContract = bpOrchestrationContract;
    this.polls = polls;
    this.activeCommitteeVotes = activeCommitteeVotes;
  }

  static VotingState initial(BlockchainAddress bpOrchestrationContract) {
    return new VotingState(bpOrchestrationContract, AvlTree.create(), CommitteeVotes.initial());
  }

  AvlTree<Hash, Poll> getPolls() {
    return polls;
  }

  /**
   * Get all poll ids and corresponding poll update hash.
   *
   * @return map of poll ids and update hashes
   */
  public AvlTree<Hash, Hash> getPollUpdateHashes() {
    AvlTree<Hash, Hash> pollIdToPollHash = AvlTree.create();
    for (Hash pollId : polls.keySet()) {
      Poll poll = getPoll(pollId);
      Hash pollHash = poll.createUpdateHash();
      pollIdToPollHash = pollIdToPollHash.set(pollId, pollHash);
    }
    return pollIdToPollHash;
  }

  BlockchainAddress getBpOrchestrationContract() {
    return bpOrchestrationContract;
  }

  VotingState createPoll(
      Hash pollId,
      BlockchainAddress pollOwner,
      String description,
      long timeoutTimestamp,
      PendingUpdate pendingUpdate) {
    Poll poll = Poll.initial(pollOwner, description, timeoutTimestamp, pendingUpdate);
    AvlTree<Hash, Poll> updatedPolls = polls.set(pollId, poll);
    return updatePolls(updatedPolls);
  }

  /**
   * Get poll from id.
   *
   * @param pollId id of poll
   * @return poll with id or null
   */
  public Poll getPoll(Hash pollId) {
    return polls.getValue(pollId);
  }

  VotingState addVote(Hash pollId, BlockchainAddress voterAddress, boolean vote, long voteTime) {
    Poll poll = getPoll(pollId);
    if (poll.isExpired(voteTime)) {
      throw new IllegalStateException("The poll timer expired");
    }
    AvlTree<Hash, Poll> updatedPolls =
        polls.set(pollId, poll.addVote(voterAddress, vote, activeCommitteeVotes));
    return updatePolls(updatedPolls);
  }

  private VotingState updatePolls(AvlTree<Hash, Poll> updatedPolls) {
    return new VotingState(bpOrchestrationContract, updatedPolls, activeCommitteeVotes);
  }

  VotingState updateActiveCommitteeVotes(CommitteeVotes newActiveCommitteeVotes) {
    return new VotingState(bpOrchestrationContract, polls, newActiveCommitteeVotes);
  }

  boolean isPartOfCommittee(BlockchainAddress voter) {
    return activeCommitteeVotes.isPartOfCommittee(voter);
  }

  /**
   * Check the result of a poll. If the poll has the result:
   *
   * <ul>
   *   <li>NO_RESULT, do nothing.
   *   <li>NO, remove the poll.
   *   <li>YES, execute the update and remove the poll.
   * </ul>
   *
   * @param pollId id of the poll
   * @param context context for executing the update
   * @return updated state
   */
  VotingState checkUpdate(Hash pollId, SysContractContext context) {
    Poll poll = getPoll(pollId);
    AvlTree<Hash, Poll> updatedPolls;
    VotingResult result = poll.getResult();
    if (result == VotingResult.NO_RESULT) {
      if (poll.isExpired(context.getBlockProductionTime())) {
        updatedPolls = polls.remove(pollId);
      } else {
        updatedPolls = polls;
      }
    } else if (result == VotingResult.YES) {
      poll.getUpdate().prepare().run(context);
      updatedPolls = polls.remove(pollId);
    } else { // result == VotingResult.NO
      updatedPolls = polls.remove(pollId);
    }
    return updatePolls(updatedPolls);
  }

  boolean hasPollFromAccount(BlockchainAddress pollOwner) {
    for (Poll poll : polls.values()) {
      if (poll.getOwner().equals(pollOwner)) {
        return true;
      }
    }
    return false;
  }

  VotingState removePoll(Hash pollId) {
    return updatePolls(polls.remove(pollId));
  }
}
