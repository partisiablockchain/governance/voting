package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.secata.stream.SafeDataInputStream;
import java.util.ArrayList;
import java.util.List;

/** The type of poll updates. */
public enum PollUpdateType {
  /** Enable feature. */
  ENABLE_FEATURE {
    @Override
    UpdateExecutor prepare(SafeDataInputStream rpc) {
      String featureFlag = rpc.readString();
      String featureValue = rpc.readString();
      return context -> context.getInvocationCreator().setFeature(featureFlag, featureValue);
    }
  },
  /**
   * Deploy contract.
   *
   * <p>Deploys the contract with provided contract address, binder, contract JAR, ABI,
   * initialization payload and cost of deployment.
   */
  DEPLOY_CONTRACT {
    @Override
    UpdateExecutor prepare(SafeDataInputStream rpc) {
      BlockchainAddress contractAddress = BlockchainAddress.read(rpc);
      byte[] binderJar = rpc.readDynamicBytes();
      byte[] contractJar = rpc.readDynamicBytes();
      byte[] abi = rpc.readDynamicBytes();
      byte[] payload = rpc.readDynamicBytes();
      long cost = rpc.readLong();
      return context ->
          context
              .getInvocationCreator()
              .deployContract(contractAddress)
              .withBinderJar(binderJar)
              .withContractJar(contractJar)
              .withAbi(abi)
              .withPayload(init -> init.write(payload))
              .allocateCost(cost)
              .sendFromContract();
    }
  },
  /**
   * Update contract.
   *
   * <p>Updates the given contract with the given update RPC.
   */
  UPDATE_CONTRACT {
    @Override
    UpdateExecutor prepare(SafeDataInputStream rpc) {
      byte[] contractJar = rpc.readDynamicBytes();
      byte[] binderJar = rpc.readDynamicBytes();
      byte[] abi = rpc.readDynamicBytes();
      byte[] updateRpc = rpc.readDynamicBytes();
      BlockchainAddress contractAddress = BlockchainAddress.read(rpc);

      return context ->
          context
              .getInvocationCreator()
              .upgradeSystemContract(contractJar, binderJar, abi, updateRpc, contractAddress);
    }
  },
  /**
   * Update plugin.
   *
   * <p>Updates the plugin of the given {@link PluginUpdateType} to the provided JAR and update RPC.
   */
  UPDATE_PLUGIN {
    @Override
    UpdateExecutor prepare(SafeDataInputStream rpc) {
      byte[] jar = rpc.readDynamicBytes();
      byte[] updateRpc = rpc.readDynamicBytes();
      PluginUpdateType pluginUpdateType = rpc.readEnum(PluginUpdateType.values());
      return context -> pluginUpdateType.updatePlugin(context, jar, updateRpc);
    }
  },
  /**
   * Update global plugin state.
   *
   * <p>Updates the global state for the plugin of the given {@link PluginUpdateType}.
   */
  UPDATE_GLOBAL_PLUGIN_STATE {
    @Override
    UpdateExecutor prepare(SafeDataInputStream rpc) {
      GlobalPluginStateUpdate globalPluginStateUpdate = GlobalPluginStateUpdate.read(rpc);
      PluginUpdateType pluginUpdateType = rpc.readEnum(PluginUpdateType.values());
      return context -> pluginUpdateType.updateGlobalPluginState(context, globalPluginStateUpdate);
    }
  },
  /**
   * Composite update.
   *
   * <p>Multiple updates of {@link PollUpdateType}
   */
  COMPOSITE_UPDATE {
    @Override
    UpdateExecutor prepare(SafeDataInputStream rpc) {
      final List<UpdateExecutor> updates = new ArrayList<>();
      long noOfUpdates = rpc.readInt();
      // execute all 'inner updates'
      for (int i = 0; i < noOfUpdates; i++) {
        PollUpdateType updateType = rpc.readEnum(PollUpdateType.values());
        updates.add(updateType.prepare(rpc));
      }
      return context -> updates.forEach(u -> u.run(context));
    }
  },
  /** Invokes a contract with a payload. */
  INVOKE_CONTRACT {
    @Override
    UpdateExecutor prepare(SafeDataInputStream rpc) {
      BlockchainAddress contractAddress = BlockchainAddress.read(rpc);
      byte[] payload = rpc.readDynamicBytes();
      long cost = rpc.readLong();
      return context ->
          context
              .getInvocationCreator()
              .invoke(contractAddress)
              .withPayload(
                  stream -> {
                    stream.write(payload);
                  })
              .allocateCost(cost)
              .sendFromContract();
    }
  },
  /** Removes the contract from payload. */
  REMOVE_CONTRACT {
    @Override
    UpdateExecutor prepare(SafeDataInputStream rpc) {
      BlockchainAddress contractAddress = BlockchainAddress.read(rpc);
      return context -> context.getInvocationCreator().removeContract(contractAddress);
    }
  },
  /** Invokes the local account plugin with a payload. */
  INVOKE_LOCAL_ACCOUNT_PLUGIN {
    @Override
    UpdateExecutor prepare(SafeDataInputStream rpc) {
      BlockchainAddress address = BlockchainAddress.read(rpc);
      byte[] payload = rpc.readDynamicBytes();
      return context ->
          context
              .getInvocationCreator()
              .updateLocalAccountPluginState(LocalPluginStateUpdate.create(address, payload));
    }
  };

  abstract UpdateExecutor prepare(SafeDataInputStream rpc);

  interface UpdateExecutor {

    void run(SysContractContext context);
  }
}
