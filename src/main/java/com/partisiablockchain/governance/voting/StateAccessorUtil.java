package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.tree.AvlTree;
import java.util.function.Function;

/** Utility class for creating objects from a StateAccessor. */
final class StateAccessorUtil {

  @SuppressWarnings("unused")
  private StateAccessorUtil() {}

  static <@ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
      AvlTree<K, V> toAvlTree(
          StateAccessor accessor, Function<StateAccessor, V> valueCreator, Class<K> keyClass) {
    AvlTree<K, V> map = AvlTree.create();
    for (StateAccessorAvlLeafNode leaf : accessor.getTreeLeaves()) {
      K key = leaf.getKey().cast(keyClass);
      V value = valueCreator.apply(leaf.getValue());
      map = map.set(key, value);
    }
    return map;
  }
}
