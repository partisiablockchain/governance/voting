package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;

/**
 * Rpc friendly pending update.
 *
 * @param pollUpdateType the enum byte of poll update type
 * @param updateRpc the update rpc
 * @param identifier identifier
 */
public record PendingUpdateRpc(PollUpdateType pollUpdateType, byte[] updateRpc, Hash identifier) {

  PendingUpdate convert() {
    return new PendingUpdate(pollUpdateType, new LargeByteArray(updateRpc), identifier);
  }
}
