package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataOutputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class VotingStateTest {

  private final BlockchainAddress firstAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private final BlockchainAddress contractAddress =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV, Hash.create(s -> s.writeInt(33)));

  private final Hash pollId =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000001");
  private final Hash updateIdentifier =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000002");

  @Test
  void getPollUpdateHashes() {
    byte[] updateRpc = SafeDataOutputStream.serialize(s -> s.writeLong(42));
    PendingUpdate pendingUpdate =
        PendingUpdate.create(PollUpdateType.DEPLOY_CONTRACT, updateRpc, updateIdentifier);
    VotingState votingState =
        VotingState.initial(contractAddress)
            .createPoll(pollId, firstAddress, "description", 1000, pendingUpdate);
    Assertions.assertThat(votingState.getPollUpdateHashes().getValue(pollId))
        .isEqualTo(votingState.getPoll(pollId).createUpdateHash());
  }

  @Test
  void createHash() {
    byte[] updateRpc = SafeDataOutputStream.serialize(s -> s.writeLong(42));
    PendingUpdate pendingUpdate =
        PendingUpdate.create(PollUpdateType.DEPLOY_CONTRACT, updateRpc, updateIdentifier);
    VotingState votingState =
        VotingState.initial(contractAddress)
            .createPoll(pollId, firstAddress, "description", 1000, pendingUpdate);
    Hash hash = votingState.getPoll(pollId).createUpdateHash();
    Assertions.assertThat(hash).isNotNull();
    Assertions.assertThat(hash)
        .isEqualTo(
            Hash.create(
                h -> {
                  firstAddress.write(h);
                  pendingUpdate.write(h);
                }));
  }
}
