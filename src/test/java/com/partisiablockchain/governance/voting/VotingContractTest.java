package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.governance.voting.VotingContract.POLL_DURATION_MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEvent;
import com.partisiablockchain.contract.ContractEventInteraction;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.contract.sys.ContractEventDeploy;
import com.partisiablockchain.contract.sys.Deploy;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.nio.charset.StandardCharsets;
import java.util.List;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class VotingContractTest {

  private final BlockchainAddress firstAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private final BlockchainAddress secondAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");

  private final BlockchainAddress addressA =
      BlockchainAddress.fromString("000000000000000000000000000000000000000003");
  private final BlockchainAddress addressB =
      BlockchainAddress.fromString("000000000000000000000000000000000000000004");
  private final BlockchainAddress addressC =
      BlockchainAddress.fromString("000000000000000000000000000000000000000005");
  private final BlockchainAddress addressD =
      BlockchainAddress.fromString("000000000000000000000000000000000000000006");
  private final BlockchainAddress addressE =
      BlockchainAddress.fromString("000000000000000000000000000000000000000007");
  private final BlockchainAddress addressF =
      BlockchainAddress.fromString("000000000000000000000000000000000000000008");

  private final Hash firstHash =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000001");
  private final Hash secondHash =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000002");
  private final Hash thirdHash =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000003");

  private final CommitteeVotes committeeVotes =
      CommitteeVotes.create(
          List.of(
              new CommitteeVote(addressA, 10),
              new CommitteeVote(addressB, 20),
              new CommitteeVote(addressC, 30)));

  private final SysContractSerialization<VotingState> serialization =
      new SysContractSerialization<>(VotingContractInvoker.class, VotingState.class);
  private SysContractContextTest context;
  private static final byte INVALID_INVOCATION_BYTE = 100;

  @BeforeEach
  public void setUp() {
    context = new SysContractContextTest(0, 0, addressA);
  }

  private CallbackContext getCallbackContext(long amount) {
    SafeDataInputStream fromBytes =
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(s -> s.writeLong(amount)));
    CallbackContext.ExecutionResult executionResult =
        CallbackContext.createResult(firstHash, true, fromBytes);
    return CallbackContext.create(FixedList.create(List.of(executionResult)));
  }

  private CallbackContext getFailedCallbackContext() {
    CallbackContext.ExecutionResult executionResult =
        CallbackContext.createResult(
            firstHash, false, SafeDataInputStream.createFromBytes(new byte[0]));
    return CallbackContext.create(FixedList.create(List.of(executionResult)));
  }

  @Test
  public void removeContractInvocation() {
    BlockchainAddress address = createContractAddress();
    CommitteeVotes newCommitteeVotes =
        CommitteeVotes.create(
            List.of(new CommitteeVote(addressA, 10), new CommitteeVote(addressB, 20)));
    VotingState initial =
        VotingState.initial(createContractAddress()).updateActiveCommitteeVotes(newCommitteeVotes);
    from(addressA);
    byte[] updateRpc = SafeDataOutputStream.serialize(address::write);
    VotingState votingWithInvokeContract =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.PROPOSE_UPDATE);
              rpc.writeString("Remove bad contract");
              rpc.writeEnum(PollUpdateType.REMOVE_CONTRACT);
              rpc.writeDynamicBytes(updateRpc);
            });
    VotingState callback =
        serialization.callback(
            context,
            getCallbackContext(VotingContract.REQUIRED_STAKED_TOKENS_FOR_UPDATE_PROPOSAL),
            votingWithInvokeContract,
            rpc -> rpc.write(context.getRemoteCalls().callbackRpc));
    Hash pollId =
        VotingContract.determinePollId(
            addressA, PollUpdateType.REMOVE_CONTRACT.ordinal(), updateRpc);
    Poll poll = callback.getPoll(pollId);
    assertThat(poll.getDescription()).isEqualTo("Remove bad contract");
    callback = callback.addVote(pollId, addressA, true, 0);
    callback = callback.addVote(pollId, addressB, true, 0);
    VotingState checkUpdateState =
        serialization.invoke(
            context,
            callback,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.CHECK_UPDATE);
              pollId.write(rpc);
            });
    assertThat(checkUpdateState.getPoll(pollId)).isNull();
    BlockchainAddress toRemove = context.getRemoveContract();
    assertThat(toRemove).isEqualTo(address);
  }

  @Test
  public void invokeContractInvocation() {
    BlockchainAddress address = createContractAddress();
    CommitteeVotes newCommitteeVotes =
        CommitteeVotes.create(
            List.of(new CommitteeVote(addressA, 10), new CommitteeVote(addressB, 20)));
    VotingState initial =
        VotingState.initial(createContractAddress()).updateActiveCommitteeVotes(newCommitteeVotes);
    from(addressA);
    final long cost = 120;
    byte[] updateRpc =
        SafeDataOutputStream.serialize(
            stream -> {
              address.write(stream);
              stream.writeDynamicBytes(new byte[] {0, 4, 2, 9});
              stream.writeLong(cost);
            });
    VotingState votingWithInvokeContract =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.PROPOSE_UPDATE);
              rpc.writeString("Deploy BYOC for SHIBA");
              rpc.writeEnum(PollUpdateType.INVOKE_CONTRACT);
              rpc.writeDynamicBytes(updateRpc);
            });
    byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] expectedRpc = AccountPluginRpc.getStakedTokens();
    assertThat(actualRpc).isEqualTo(expectedRpc);
    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    VotingState callback =
        serialization.callback(
            context,
            getCallbackContext(VotingContract.REQUIRED_STAKED_TOKENS_FOR_UPDATE_PROPOSAL),
            votingWithInvokeContract,
            rpc -> rpc.write(actualCallbackRpc));
    Hash pollId =
        VotingContract.determinePollId(
            addressA, PollUpdateType.INVOKE_CONTRACT.ordinal(), updateRpc);
    Poll poll = callback.getPoll(pollId);
    assertThat(poll.getDescription()).isEqualTo("Deploy BYOC for SHIBA");
    callback = callback.addVote(pollId, addressA, true, 0);
    callback = callback.addVote(pollId, addressB, true, 0);
    VotingState checkUpdateState =
        serialization.invoke(
            context,
            callback,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.CHECK_UPDATE);
              pollId.write(rpc);
            });
    assertThat(checkUpdateState.getPoll(pollId)).isNull();
    List<ContractEvent> contractEvents = context.getInteractions();
    assertThat(contractEvents.size()).isEqualTo(1);
    ContractEventInteraction event = (ContractEventInteraction) contractEvents.get(0);
    assertThat(event.contract).isEqualTo(address);
    assertThat(event.allocatedCost).isEqualTo(cost);
    assertThat(SafeDataOutputStream.serialize(event.rpc))
        .isEqualTo(
            SafeDataOutputStream.serialize(
                s -> {
                  s.write(new byte[] {0, 4, 2, 9});
                }));
  }

  @Test
  public void create() {
    BlockchainAddress bpOrchestrationContractAddress = createContractAddress();
    VotingState state = serialization.create(context, bpOrchestrationContractAddress::write);
    assertThat(state).isNotNull();
    assertThat(state.getBpOrchestrationContract()).isEqualTo(bpOrchestrationContractAddress);
  }

  @Test
  public void createPoll_bpOrchestrationContract() {
    BlockchainAddress contract = createContractAddress();
    VotingState state = createEnableFeaturePollState(firstAddress, VotingState.initial(contract));
    assertThat(state.getBpOrchestrationContract()).isEqualTo(contract);
  }

  @Test
  public void invokeInvalidInvocation() {
    VotingState state =
        createEnableFeaturePollState(firstAddress, VotingState.initial(createContractAddress()));
    assertThatThrownBy(
            () ->
                serialization.invoke(context, state, rpc -> rpc.writeByte(INVALID_INVOCATION_BYTE)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid shortname: 100");
  }

  @Test
  public void invokeOnUpgradeSimple() {
    BlockchainAddress contractAddress = createContractAddress();
    VotingState initialState =
        createEnableFeaturePollState(
            firstAddress,
            VotingState.initial(contractAddress).updateActiveCommitteeVotes(committeeVotes));
    initialState = initialState.addVote(firstHash, addressB, true, 0);
    VotingContract contract = new VotingContract();

    VotingState updatedState = contract.upgrade(StateAccessor.create(initialState));

    assertThat(updatedState.getBpOrchestrationContract())
        .isEqualTo(initialState.getBpOrchestrationContract());

    AvlTree<Hash, Poll> updatedPollTree = updatedState.getPolls();

    assertThat(updatedPollTree.size()).isEqualTo(1);
    Poll poll = updatedPollTree.getValue(firstHash);
    assertThat(poll.getOwner()).isEqualTo(firstAddress);
    assertThat(poll.getDescription()).isEqualTo("A description");
    assertThat(poll.getTimeoutTimestamp()).isEqualTo(0);
    assertThat(poll.getUpdate())
        .usingRecursiveComparison()
        .isEqualTo(
            PendingUpdate.create(
                PollUpdateType.ENABLE_FEATURE,
                SafeDataOutputStream.serialize(
                    rpc -> {
                      rpc.writeString("FEATURE_KEY");
                      rpc.writeString("FEATURE_VALUE");
                    }),
                secondHash));
    assertThat(poll.getResult()).isEqualTo(VotingResult.NO_RESULT);
    assertThat(poll.hasVoteFromAccount(addressB)).isTrue();

    assertThat(initialState.isPartOfCommittee(addressA))
        .isEqualTo(updatedState.isPartOfCommittee(addressA));
    assertThat(initialState.isPartOfCommittee(addressD))
        .isEqualTo(updatedState.isPartOfCommittee(addressD));
  }

  @Test
  public void invokeVote_Invalid() {
    from(addressC);
    BlockchainAddress contract = createContractAddress();
    VotingState state =
        createEnableFeaturePollState(
            firstAddress, VotingState.initial(contract).updateActiveCommitteeVotes(committeeVotes));
    setBlockProductionTime(1);
    ThrowableAssert.ThrowingCallable pollExpired =
        () ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(VotingContract.Invocations.VOTE);
                  firstHash.write(rpc);
                  rpc.writeBoolean(true);
                });
    assertThatThrownBy(pollExpired)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("The poll timer expired");

    from(firstAddress);
    ThrowableAssert.ThrowingCallable notInCommittee =
        () ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(VotingContract.Invocations.VOTE);
                  firstHash.write(rpc);
                  rpc.writeBoolean(true);
                });
    assertThatThrownBy(notInCommittee)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only block producers can vote for updates");
  }

  @Test
  public void invokeVote_DifferentResults() {
    BlockchainAddress contract = createContractAddress();
    CommitteeVotes newCommitteeVotes =
        CommitteeVotes.create(
            List.of(
                new CommitteeVote(addressA, 10),
                new CommitteeVote(addressB, 20),
                new CommitteeVote(addressC, 30),
                new CommitteeVote(addressD, 40),
                new CommitteeVote(addressE, 1),
                new CommitteeVote(addressF, 1)));
    VotingState initialWithCommittee =
        VotingState.initial(contract).updateActiveCommitteeVotes(newCommitteeVotes);
    VotingState state = createEnableFeaturePollState(firstAddress, initialWithCommittee);

    // NO_RESULT
    from(addressA);
    VotingState votingStateA =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.VOTE);
              firstHash.write(rpc);
              rpc.writeBoolean(true);
            });
    VotingResult noResultA = votingStateA.getPoll(firstHash).getResult();
    assertThat(noResultA).isEqualTo(VotingResult.NO_RESULT);

    from(addressB);
    VotingState votingStateB =
        serialization.invoke(
            context,
            votingStateA,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.VOTE);
              firstHash.write(rpc);
              rpc.writeBoolean(false);
            });
    VotingResult noResultB = votingStateB.getPoll(firstHash).getResult();
    assertThat(noResultB).isEqualTo(VotingResult.NO_RESULT);

    from(addressC);
    VotingState votingStateC =
        serialization.invoke(
            context,
            votingStateB,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.VOTE);
              firstHash.write(rpc);
              rpc.writeBoolean(true);
            });
    VotingResult yesD = votingStateC.getPoll(firstHash).getResult();
    assertThat(yesD).isEqualTo(VotingResult.NO_RESULT);

    // NO
    from(addressE);
    VotingState votingStateE =
        serialization.invoke(
            context,
            votingStateB,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.VOTE);
              firstHash.write(rpc);
              rpc.writeBoolean(false);
            });
    from(addressF);
    VotingState votingStateF =
        serialization.invoke(
            context,
            votingStateE,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.VOTE);
              firstHash.write(rpc);
              rpc.writeBoolean(false);
            });
    VotingResult noF = votingStateF.getPoll(firstHash).getResult();
    assertThat(noF).isEqualTo(VotingResult.NO);

    // YES
    from(addressE);
    VotingState votingStateYesE =
        serialization.invoke(
            context,
            votingStateC,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.VOTE);
              firstHash.write(rpc);
              rpc.writeBoolean(true);
            });
    from(addressF);
    VotingState votingStateYesF =
        serialization.invoke(
            context,
            votingStateYesE,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.VOTE);
              firstHash.write(rpc);
              rpc.writeBoolean(true);
            });
    VotingResult yesF = votingStateYesF.getPoll(firstHash).getResult();
    assertThat(yesF).isEqualTo(VotingResult.YES);
  }

  @Test
  public void invokeVote_ExactlyHalfWeightedYes() {
    BlockchainAddress contract = createContractAddress();
    CommitteeVotes newCommitteeVotes =
        CommitteeVotes.create(
            List.of(
                new CommitteeVote(addressA, 1),
                new CommitteeVote(addressB, 1),
                new CommitteeVote(addressC, 2),
                new CommitteeVote(addressD, 1)));
    VotingState initialWithCommittee =
        VotingState.initial(contract).updateActiveCommitteeVotes(newCommitteeVotes);
    VotingState state = createEnableFeaturePollState(firstAddress, initialWithCommittee);

    // NO_RESULT
    from(addressA);
    VotingState votingStateA =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.VOTE);
              firstHash.write(rpc);
              rpc.writeBoolean(true);
            });
    from(addressB);
    VotingState votingStateB =
        serialization.invoke(
            context,
            votingStateA,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.VOTE);
              firstHash.write(rpc);
              rpc.writeBoolean(true);
            });
    from(addressC);
    VotingState votingStateC =
        serialization.invoke(
            context,
            votingStateB,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.VOTE);
              firstHash.write(rpc);
              rpc.writeBoolean(false);
            });
    VotingResult noResultA = votingStateC.getPoll(firstHash).getResult();
    assertThat(noResultA).isEqualTo(VotingResult.NO_RESULT);
  }

  private VotingState createEnableFeaturePollState(
      BlockchainAddress pollOwnerAddress, VotingState initial) {
    return initial.createPoll(
        firstHash,
        pollOwnerAddress,
        "A description",
        0,
        PendingUpdate.create(
            PollUpdateType.ENABLE_FEATURE,
            SafeDataOutputStream.serialize(
                rpc -> {
                  rpc.writeString("FEATURE_KEY");
                  rpc.writeString("FEATURE_VALUE");
                }),
            secondHash));
  }

  @Test
  public void invokeProposeUpdate() {
    long blockProductionTime = 1000L;
    setBlockProductionTime(blockProductionTime);
    from(secondAddress);
    VotingState state =
        createEnableFeaturePollState(firstAddress, VotingState.initial(createContractAddress()));
    String featureKey = "FEATURE_KEY";
    String featureValue = "FEATURE_VALUE";
    byte[] enableFeatureRpc =
        SafeDataOutputStream.serialize(
            rpc -> {
              rpc.writeString(featureKey);
              rpc.writeString(featureValue);
            });
    String description = "A description";
    VotingState newState =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.PROPOSE_UPDATE);
              rpc.writeString(description);
              rpc.writeEnum(PollUpdateType.ENABLE_FEATURE);
              rpc.writeDynamicBytes(enableFeatureRpc);
            });
    assertThat(newState.getPolls().size()).isEqualTo(1);
    byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] expectedRpc = AccountPluginRpc.getStakedTokens();
    assertThat(actualRpc).isEqualTo(expectedRpc);
    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    PendingUpdate pendingUpdate =
        PendingUpdate.create(
            PollUpdateType.ENABLE_FEATURE, enableFeatureRpc, context.getCurrentTransactionHash());
    Hash pollId =
        Hash.create(
            s -> {
              context.getFrom().write(s);
              s.writeByte(PollUpdateType.ENABLE_FEATURE.ordinal());
              s.write(enableFeatureRpc);
            });
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            VotingContract.Callbacks.proposeUpdate(
                pollId, secondAddress, description, pendingUpdate));
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);

    VotingState justEnoughStakedTokensState =
        serialization.callback(
            context,
            getCallbackContext(VotingContract.REQUIRED_STAKED_TOKENS_FOR_UPDATE_PROPOSAL),
            state,
            rpc -> {
              rpc.writeByte(VotingContract.Callbacks.PROPOSE_UPDATE);
              pollId.write(rpc);
              firstAddress.write(rpc);
              rpc.writeString("A description");
              pendingUpdate.write(rpc);
            });
    assertThat(justEnoughStakedTokensState).isNotNull();
    assertThat(justEnoughStakedTokensState.getPolls().size()).isEqualTo(2);
    Poll pollCreate = justEnoughStakedTokensState.getPoll(pollId);
    assertThat(pollCreate.getDescription()).isEqualTo("A description");
    assertThat(pollCreate.getTimeoutTimestamp())
        .isEqualTo(blockProductionTime + POLL_DURATION_MILLIS);
    PendingUpdate update = pollCreate.getUpdate();
    assertThat(update).isNotNull();
    assertThat(update).usingRecursiveComparison().isEqualTo(pendingUpdate);
  }

  @Test
  public void invokeProposeUpdate_MalFormedPayload() {
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    VotingState.initial(firstAddress),
                    rpc -> {
                      rpc.writeByte(VotingContract.Invocations.PROPOSE_UPDATE);
                      rpc.writeString("A description");
                      rpc.writeEnum(PollUpdateType.ENABLE_FEATURE);
                      rpc.writeDynamicBytes(new byte[9]);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Entire content of stream was not read");
  }

  @Test
  public void invokeProposeUpdate_Invalid() {
    long blockProductionTime = 1000;
    setBlockProductionTime(blockProductionTime);
    String featureKey = "FEATURE_KEY";
    String featureValue = "FEATURE_VALUE";
    byte[] updateRpc =
        SafeDataOutputStream.serialize(
            rpc -> {
              rpc.writeString(featureKey);
              rpc.writeString(featureValue);
            });
    VotingState twoPollState =
        VotingState.initial(firstAddress)
            .createPoll(
                firstHash,
                firstAddress,
                "description",
                blockProductionTime + 1,
                PendingUpdate.create(PollUpdateType.ENABLE_FEATURE, updateRpc, thirdHash))
            .createPoll(
                secondHash,
                secondAddress,
                "description2",
                blockProductionTime + 1,
                PendingUpdate.create(PollUpdateType.ENABLE_FEATURE, updateRpc, thirdHash));
    from(secondAddress);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    twoPollState,
                    rpc -> {
                      rpc.writeByte(VotingContract.Invocations.PROPOSE_UPDATE);
                      firstHash.write(rpc);
                      rpc.writeString("A description");
                      rpc.writeEnum(PollUpdateType.ENABLE_FEATURE);
                      rpc.writeDynamicBytes(updateRpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Each account can have a maximum of 1 pending proposals");

    ThrowableAssert.ThrowingCallable tooFewStakedTokens =
        () ->
            serialization.callback(
                context,
                getCallbackContext(VotingContract.REQUIRED_STAKED_TOKENS_FOR_UPDATE_PROPOSAL - 1),
                twoPollState,
                rpc -> {
                  rpc.writeByte(VotingContract.Callbacks.PROPOSE_UPDATE);
                  firstHash.write(rpc);
                  secondAddress.write(rpc);
                  rpc.writeString("A description");
                  rpc.writeEnum(PollUpdateType.ENABLE_FEATURE);
                  rpc.writeDynamicBytes(updateRpc);
                  firstHash.write(rpc);
                });
    assertThatThrownBy(tooFewStakedTokens)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Too few staked tokens for update proposal");

    VotingState unchangedVotingState =
        serialization.callback(
            context,
            getFailedCallbackContext(),
            twoPollState,
            rpc -> {
              rpc.writeByte(VotingContract.Callbacks.PROPOSE_UPDATE);
              thirdHash.write(rpc);
              secondAddress.write(rpc);
              rpc.writeString("A description");
              rpc.writeEnum(PollUpdateType.ENABLE_FEATURE);
              rpc.writeDynamicBytes(updateRpc);
              thirdHash.write(rpc);
            });
    assertThat(unchangedVotingState.getPoll(thirdHash)).isNull();
  }

  @Test
  public void invokeCheckUpdate_Invalid() {
    long blockProductionTime = 1000L;
    setBlockProductionTime(blockProductionTime);

    assertThatThrownBy(() -> checkUpdate(VotingState.initial(firstAddress)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No poll exists for the given poll id");

    VotingState noResultPollState = stateWillPollForFeatureFlag(blockProductionTime);
    assertThat(noResultPollState.getPoll(firstHash).getResult()).isEqualTo(VotingResult.NO_RESULT);
    VotingState noResultCheckUpdateState = checkUpdate(noResultPollState);
    assertThat(noResultCheckUpdateState.getPoll(firstHash)).isNotNull();

    VotingState noPollState =
        noResultPollState
            .addVote(firstHash, addressC, false, blockProductionTime)
            .addVote(firstHash, addressA, false, blockProductionTime);
    assertThat(noPollState.getPoll(firstHash).getResult()).isEqualTo(VotingResult.NO);
    VotingState noPollCheckUpdateState = checkUpdate(noPollState);
    assertThat(noPollCheckUpdateState.getPoll(firstHash)).isNull();
    assertThat(context.getInteractions().size()).isEqualTo(0);
  }

  private VotingState stateWillPollForFeatureFlag(long timeoutTimestamp) {
    String featureKey = "FEATURE_KEY";
    String featureValue = "FEATURE_VALUE";
    return createPollWithPendingUpdate(
        PendingUpdate.create(
            PollUpdateType.ENABLE_FEATURE,
            SafeDataOutputStream.serialize(
                rpc -> {
                  rpc.writeString(featureKey);
                  rpc.writeString(featureValue);
                }),
            secondHash),
        timeoutTimestamp);
  }

  @Test
  public void invokeRemovePoll() {
    VotingState votingState = stateWillPollForFeatureFlag(0);
    from(firstAddress);
    VotingState newState = invokeRemovePoll(votingState);
    assertThat(newState.getPolls().size()).isEqualTo(0);
  }

  private VotingState invokeRemovePoll(VotingState votingState) {
    return serialization.invoke(
        context,
        votingState,
        rpc -> {
          rpc.writeByte(VotingContract.Invocations.REMOVE_POLL);
          firstHash.write(rpc);
        });
  }

  @Test
  public void invokeRemovePoll_Invalid() {
    assertThatThrownBy(() -> invokeRemovePoll(VotingState.initial(createContractAddress())))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No poll exists for the given poll id");

    from(secondAddress);
    assertThatThrownBy(() -> invokeRemovePoll(stateWillPollForFeatureFlag(0)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the owner can remove a poll");
  }

  @Test
  public void invokeCheckUpdate_CompositeUpdate() {
    setBlockProductionTime(1000);

    // ENABLE_FEATURE setup
    final String featureKey = "FEATURE_KEY";
    final String featureValue = "FEATURE_VALUE";
    final String featureKey2 = "FEATURE_KEY_2";
    final String featureValue2 = "FEATURE_VALUE_2";
    // PLUGIN_UPDATE setup
    final byte[] updatePluginJar = "updatePluginJar".getBytes(StandardCharsets.UTF_8);
    final byte[] updatePluginRpc = "updatePluginRpc".getBytes(StandardCharsets.UTF_8);
    // DEPLOY_CONTRACT setup
    final BlockchainAddress contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_PUBLIC,
            Hash.create(rpc -> rpc.writeString("TestDeployContract")));
    final byte[] binderJar =
        SafeDataOutputStream.serialize(stream -> stream.writeString("binderJar"));
    final byte[] contractJar =
        SafeDataOutputStream.serialize(stream -> stream.writeString("contractJar"));
    final byte[] contractAbi = "contractAbi".getBytes(StandardCharsets.UTF_8);
    final byte[] payload = SafeDataOutputStream.serialize(stream -> stream.writeString("payload"));
    final long cost = 100;
    // UPDATE_GLOBAL_PLUGIN_STATE setup
    checkGlobalPluginStateUpdate(PluginUpdateType.ACCOUNT);
    checkGlobalPluginStateUpdate(PluginUpdateType.CONSENSUS);
    final byte[] stateUpdateRpc = "stateUpdateRpc".getBytes(StandardCharsets.UTF_8);
    final GlobalPluginStateUpdate pluginStateUpdate =
        GlobalPluginStateUpdate.create(stateUpdateRpc);
    // UPDATE_CONTRACT setup
    final BlockchainAddress contractAddressUpdate =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_PUBLIC,
            Hash.create(rpc -> rpc.writeString("TestUpdateContract")));
    final byte[] binderJarUpdate = "binderJar".getBytes(StandardCharsets.UTF_8);
    final byte[] contractJarUpdate = "contractJar".getBytes(StandardCharsets.UTF_8);
    final byte[] updateContractRpc = "updateContractRpc".getBytes(StandardCharsets.UTF_8);

    VotingState compositeState =
        createPollWithPendingUpdate(
                PendingUpdate.create(
                    PollUpdateType.COMPOSITE_UPDATE,
                    SafeDataOutputStream.serialize(
                        rpc -> {
                          rpc.writeInt(6);
                          rpc.writeEnum(PollUpdateType.ENABLE_FEATURE);
                          rpc.writeString(featureKey);
                          rpc.writeString(featureValue);

                          rpc.writeEnum(PollUpdateType.UPDATE_PLUGIN);
                          rpc.writeDynamicBytes(updatePluginJar);
                          rpc.writeDynamicBytes(updatePluginRpc);
                          rpc.writeEnum(PluginUpdateType.ROUTING);

                          rpc.writeEnum(PollUpdateType.DEPLOY_CONTRACT);
                          contractAddress.write(rpc);
                          rpc.writeDynamicBytes(binderJar);
                          rpc.writeDynamicBytes(contractJar);
                          rpc.writeDynamicBytes(contractAbi);
                          rpc.writeDynamicBytes(payload);
                          rpc.writeLong(cost);

                          rpc.writeEnum(PollUpdateType.UPDATE_GLOBAL_PLUGIN_STATE);
                          pluginStateUpdate.write(rpc);
                          rpc.writeEnum(PluginUpdateType.ACCOUNT);

                          rpc.writeEnum(PollUpdateType.UPDATE_CONTRACT);
                          rpc.writeDynamicBytes(contractJarUpdate);
                          rpc.writeDynamicBytes(binderJarUpdate);
                          rpc.writeDynamicBytes(contractAbi);
                          rpc.writeDynamicBytes(updateContractRpc);
                          contractAddressUpdate.write(rpc);

                          rpc.writeEnum(PollUpdateType.ENABLE_FEATURE);
                          rpc.writeString(featureKey2);
                          rpc.writeString(featureValue2);
                        }),
                    secondHash),
                0)
            .addVote(firstHash, addressB, true, 0)
            .addVote(firstHash, addressC, true, 0);
    assertThat(compositeState.getPoll(firstHash).getResult()).isEqualTo(VotingResult.YES);
    VotingState checkUpdateState =
        serialization.invoke(
            context,
            compositeState,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.CHECK_UPDATE);
              firstHash.write(rpc);
            });
    assertThat(checkUpdateState.getPoll(firstHash)).isNull();

    // ENABLE_FEATURE test
    assertThat(context.getFeature(featureKey)).isEqualTo(featureValue);
    assertThat(context.getFeature(featureKey2)).isEqualTo(featureValue2);

    // PLUGIN_UPDATE test
    assertThat(context.getUpdateRoutingPluginJar()).isEqualTo(updatePluginJar);
    assertThat(context.getUpdateRoutingPluginRpc()).isEqualTo(updatePluginRpc);

    // DEPLOY_CONTRACT test
    ContractEventDeploy deployEvent = (ContractEventDeploy) context.getInteractions().get(0);
    Deploy deploy = deployEvent.deploy;
    assertThat(deploy.contract).isEqualTo(contractAddress);
    assertThat(deploy.contractJar).isEqualTo(contractJar);
    assertThat(deploy.binderJar).isEqualTo(binderJar);
    assertThat(deploy.abi).isEqualTo(contractAbi);
    assertThat(SafeDataOutputStream.serialize(deploy.rpc)).isEqualTo(payload);
    assertThat(deployEvent.allocatedCost).isEqualTo(cost);

    // UPDATE_GLOBAL_PLUGIN_STATE test
    assertThat(context.getUpdateGlobalConsensusPluginState().getRpc()).isEqualTo(stateUpdateRpc);

    // UPDATE_CONTRACT test
    assertThat(context.getUpgradeSystemContractContractAddress()).isEqualTo(contractAddressUpdate);
    assertThat(context.getUpgradeSystemContractContractJar()).isEqualTo(contractJarUpdate);
    assertThat(context.getUpgradeSystemContractBinderJar()).isEqualTo(binderJarUpdate);
    assertThat(context.getUpgradeSystemContractRpc()).isEqualTo(updateContractRpc);
  }

  @Test
  public void invokeCheckUpdate_CompositeUpdateForIllegalState() {
    setBlockProductionTime(1000);
    checkGlobalPluginStateUpdate(PluginUpdateType.ACCOUNT);
    checkGlobalPluginStateUpdate(PluginUpdateType.CONSENSUS);
    checkGlobalPluginStateUpdate(PluginUpdateType.SHARED_OBJECT_STORE);

    byte[] stateUpdateRpc = "stateUpdateRpc".getBytes(StandardCharsets.UTF_8);
    GlobalPluginStateUpdate pluginStateUpdate = GlobalPluginStateUpdate.create(stateUpdateRpc);
    VotingState pluginUpdateState =
        createPollWithPendingUpdate(
                PendingUpdate.create(
                    PollUpdateType.COMPOSITE_UPDATE,
                    SafeDataOutputStream.serialize(
                        rpc -> {
                          rpc.writeInt(1);
                          rpc.writeEnum(PollUpdateType.UPDATE_GLOBAL_PLUGIN_STATE);
                          pluginStateUpdate.write(rpc);
                          rpc.writeEnum(PluginUpdateType.ROUTING);
                        }),
                    secondHash),
                0)
            .addVote(firstHash, addressC, true, 0)
            .addVote(firstHash, addressB, true, 0);
    assertThat(pluginUpdateState.getPoll(firstHash).getResult()).isEqualTo(VotingResult.YES);
    assertThatThrownBy(() -> checkUpdate(pluginUpdateState))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("The global plugin state of a routing plugin cannot be updated");
  }

  @Test
  public void invokeCheckUpdate_CompositeUpdateInCompositeUpdate() {
    setBlockProductionTime(1000);
    String featureKey = "COMPOSITE_KEY";
    String featureValue = "COMPOSITE_VALUE";
    VotingState compositeCompositeState =
        createPollWithPendingUpdate(
                PendingUpdate.create(
                    PollUpdateType.COMPOSITE_UPDATE,
                    SafeDataOutputStream.serialize(
                        rpc -> {
                          rpc.writeInt(1);
                          rpc.writeEnum(PollUpdateType.COMPOSITE_UPDATE);
                          rpc.writeInt(1);
                          rpc.writeEnum(PollUpdateType.ENABLE_FEATURE);
                          rpc.writeString(featureKey);
                          rpc.writeString(featureValue);
                        }),
                    secondHash),
                0)
            .addVote(firstHash, addressB, true, 0)
            .addVote(firstHash, addressC, true, 0);
    assertThat(compositeCompositeState.getPoll(firstHash).getResult()).isEqualTo(VotingResult.YES);
    VotingState checkUpdateState =
        serialization.invoke(
            context,
            compositeCompositeState,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.CHECK_UPDATE);
              firstHash.write(rpc);
            });
    assertThat(context.getFeature(featureKey)).isEqualTo(featureValue);
    assertThat(checkUpdateState.getPoll(firstHash)).isNull();
  }

  @Test
  public void invokeCheckUpdate_EnableFeature() {
    setBlockProductionTime(1000);

    String featureKey = "FEATURE_KEY";
    String featureValue = "FEATURE_VALUE";
    VotingState enableFeatureState =
        createPollWithPendingUpdate(
                PendingUpdate.create(
                    PollUpdateType.ENABLE_FEATURE,
                    SafeDataOutputStream.serialize(
                        rpc -> {
                          rpc.writeString(featureKey);
                          rpc.writeString(featureValue);
                        }),
                    secondHash),
                0)
            .addVote(firstHash, addressB, true, 0)
            .addVote(firstHash, addressC, true, 0);
    assertThat(enableFeatureState.getPoll(firstHash).getResult()).isEqualTo(VotingResult.YES);
    VotingState checkUpdateState =
        serialization.invoke(
            context,
            enableFeatureState,
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.CHECK_UPDATE);
              firstHash.write(rpc);
            });
    assertThat(context.getFeature(featureKey)).isEqualTo(featureValue);
    assertThat(checkUpdateState.getPoll(firstHash)).isNull();
  }

  @Test
  public void invokeCheckUpdate_DeployContract() {
    setBlockProductionTime(1000);

    BlockchainAddress contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_PUBLIC,
            Hash.create(rpc -> rpc.writeString("TestDeployContract")));
    byte[] binderJar = SafeDataOutputStream.serialize(stream -> stream.writeString("binderJar"));
    byte[] contractJar =
        SafeDataOutputStream.serialize(stream -> stream.writeString("contractJar"));
    byte[] contractAbi =
        SafeDataOutputStream.serialize(stream -> stream.writeString("contractAbi"));
    byte[] payload = SafeDataOutputStream.serialize(stream -> stream.writeString("payload"));
    long cost = 100;
    PendingUpdate pendingUpdate =
        PendingUpdate.create(
            PollUpdateType.DEPLOY_CONTRACT,
            SafeDataOutputStream.serialize(
                rpc -> {
                  contractAddress.write(rpc);
                  rpc.writeDynamicBytes(binderJar);
                  rpc.writeDynamicBytes(contractJar);
                  rpc.writeDynamicBytes(contractAbi);
                  rpc.writeDynamicBytes(payload);
                  rpc.writeLong(cost);
                }),
            secondHash);
    VotingState state =
        createPollWithPendingUpdate(pendingUpdate, 0)
            .addVote(firstHash, addressC, true, 0)
            .addVote(firstHash, addressB, true, 0);
    assertThat(state.getPoll(firstHash).getResult()).isEqualTo(VotingResult.YES);
    VotingState checkUpdateState = checkUpdate(state);

    ContractEventDeploy deployEvent = (ContractEventDeploy) context.getInteractions().get(0);
    Deploy deploy = deployEvent.deploy;
    assertThat(deploy.contract).isEqualTo(contractAddress);
    assertThat(deploy.contractJar).isEqualTo(contractJar);
    assertThat(deploy.binderJar).isEqualTo(binderJar);
    assertThat(deploy.abi).isEqualTo(contractAbi);
    assertThat(SafeDataOutputStream.serialize(deploy.rpc)).isEqualTo(payload);
    assertThat(deployEvent.allocatedCost).isEqualTo(cost);
    assertThat(checkUpdateState.getPoll(firstHash)).isNull();
  }

  @Test
  public void invokeCheckUpdate_UpdatePlugin() {
    setBlockProductionTime(1000);

    byte[] updatePluginJar = "updatePluginJar".getBytes(StandardCharsets.UTF_8);
    byte[] updatePluginRpc = "updatePluginRpc".getBytes(StandardCharsets.UTF_8);
    for (PluginUpdateType pluginUpdateType : PluginUpdateType.values()) {
      PendingUpdate pendingUpdate =
          PendingUpdate.create(
              PollUpdateType.UPDATE_PLUGIN,
              SafeDataOutputStream.serialize(
                  rpc -> {
                    rpc.writeDynamicBytes(updatePluginJar);
                    rpc.writeDynamicBytes(updatePluginRpc);
                    rpc.writeEnum(pluginUpdateType);
                  }),
              secondHash);
      VotingState pluginUpdateState =
          createPollWithPendingUpdate(pendingUpdate, 0)
              .addVote(firstHash, addressC, true, 0)
              .addVote(firstHash, addressB, true, 0);
      assertThat(pluginUpdateState.getPoll(firstHash).getResult()).isEqualTo(VotingResult.YES);
      VotingState checkUpdateState = checkUpdate(pluginUpdateState);
      assertThat(checkUpdateState.getPoll(firstHash)).isNull();
      if (pluginUpdateType == PluginUpdateType.ACCOUNT) {
        assertThat(context.getUpdateAccountPluginJar()).isEqualTo(updatePluginJar);
        assertThat(context.getUpdateAccountPluginRpc()).isEqualTo(updatePluginRpc);
      } else if (pluginUpdateType == PluginUpdateType.ROUTING) {
        assertThat(context.getUpdateRoutingPluginJar()).isEqualTo(updatePluginJar);
        assertThat(context.getUpdateRoutingPluginRpc()).isEqualTo(updatePluginRpc);
      } else if (pluginUpdateType == PluginUpdateType.CONSENSUS) {
        assertThat(context.getUpdateConsensusPluginJar()).isEqualTo(updatePluginJar);
        assertThat(context.getUpdateConsensusPluginRpc()).isEqualTo(updatePluginRpc);
      } else { // SHARED_OBJECT_STORE
        assertThat(context.getUpdateSharedObjectStorePluginJar()).isEqualTo(updatePluginJar);
        assertThat(context.getUpdateSharedObjectStorePluginRpc()).isEqualTo(updatePluginRpc);
      }
    }
  }

  @Test
  public void invokeCheckUpdate_UpdateContract() {
    setBlockProductionTime(1000);
    BlockchainAddress contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_PUBLIC,
            Hash.create(rpc -> rpc.writeString("TestUpdateContract")));
    byte[] binderJar = "binderJar".getBytes(StandardCharsets.UTF_8);
    byte[] contractJar = "contractJar".getBytes(StandardCharsets.UTF_8);
    byte[] updateContractRpc = "updateContractRpc".getBytes(StandardCharsets.UTF_8);

    byte[] abi = SafeDataOutputStream.serialize(s -> s.writeString("UpdateContractAbi"));
    PendingUpdate pendingUpdateWithAbi =
        PendingUpdate.create(
            PollUpdateType.UPDATE_CONTRACT,
            SafeDataOutputStream.serialize(
                rpc -> {
                  rpc.writeDynamicBytes(contractJar);
                  rpc.writeDynamicBytes(binderJar);
                  rpc.writeDynamicBytes(abi);
                  rpc.writeDynamicBytes(updateContractRpc);
                  contractAddress.write(rpc);
                }),
            secondHash);

    VotingState stateWithAbi =
        createPollWithPendingUpdate(pendingUpdateWithAbi, 0)
            .addVote(firstHash, addressC, true, 0)
            .addVote(firstHash, addressB, true, 0);
    assertThat(stateWithAbi.getPoll(firstHash).getResult()).isEqualTo(VotingResult.YES);

    VotingState checkUpdateStateWithAbi = checkUpdate(stateWithAbi);
    assertThat(context.getUpgradeSystemContractContractAddress()).isEqualTo(contractAddress);
    assertThat(context.getUpgradeSystemContractContractJar()).isEqualTo(contractJar);
    assertThat(context.getUpgradeSystemContractBinderJar()).isEqualTo(binderJar);
    assertThat(context.getUpgradeSystemContractAbi()).isEqualTo(abi);
    assertThat(context.getUpgradeSystemContractRpc()).isEqualTo(updateContractRpc);
    assertThat(checkUpdateStateWithAbi.getPoll(firstHash)).isNull();
  }

  @Test
  public void invokeCheckUpdate_UpdateGlobalPluginState() {
    setBlockProductionTime(1000);
    checkGlobalPluginStateUpdate(PluginUpdateType.ACCOUNT);
    checkGlobalPluginStateUpdate(PluginUpdateType.CONSENSUS);
    checkGlobalPluginStateUpdate(PluginUpdateType.SHARED_OBJECT_STORE);

    byte[] stateUpdateRpc = "stateUpdateRpc".getBytes(StandardCharsets.UTF_8);
    GlobalPluginStateUpdate pluginStateUpdate = GlobalPluginStateUpdate.create(stateUpdateRpc);
    VotingState pluginUpdateState =
        createPollWithPendingUpdate(
                PendingUpdate.create(
                    PollUpdateType.UPDATE_GLOBAL_PLUGIN_STATE,
                    SafeDataOutputStream.serialize(
                        rpc -> {
                          pluginStateUpdate.write(rpc);
                          rpc.writeEnum(PluginUpdateType.ROUTING);
                        }),
                    secondHash),
                0)
            .addVote(firstHash, addressC, true, 0)
            .addVote(firstHash, addressB, true, 0);
    assertThat(pluginUpdateState.getPoll(firstHash).getResult()).isEqualTo(VotingResult.YES);
    assertThatThrownBy(() -> checkUpdate(pluginUpdateState))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("The global plugin state of a routing plugin cannot be updated");
  }

  private void checkGlobalPluginStateUpdate(PluginUpdateType pluginUpdateType) {
    byte[] stateUpdateRpc = "stateUpdateRpc".getBytes(StandardCharsets.UTF_8);
    GlobalPluginStateUpdate pluginStateUpdate = GlobalPluginStateUpdate.create(stateUpdateRpc);

    PendingUpdate pendingUpdate =
        PendingUpdate.create(
            PollUpdateType.UPDATE_GLOBAL_PLUGIN_STATE,
            SafeDataOutputStream.serialize(
                rpc -> {
                  pluginStateUpdate.write(rpc);
                  rpc.writeEnum(pluginUpdateType);
                }),
            secondHash);
    VotingState pluginUpdateState =
        createPollWithPendingUpdate(pendingUpdate, 0)
            .addVote(firstHash, addressC, true, 0)
            .addVote(firstHash, addressB, true, 0);
    assertThat(pluginUpdateState.getPoll(firstHash).getResult()).isEqualTo(VotingResult.YES);
    VotingState checkUpdateState = checkUpdate(pluginUpdateState);
    assertThat(checkUpdateState.getPoll(firstHash)).isNull();
    if (pluginUpdateType == PluginUpdateType.CONSENSUS) {
      assertThat(context.getUpdateGlobalConsensusPluginState().getRpc()).isEqualTo(stateUpdateRpc);
    } else if (pluginUpdateType == PluginUpdateType.ACCOUNT) {
      assertThat(context.getUpdateGlobalAccountPluginState().getRpc()).isEqualTo(stateUpdateRpc);
    } else { // SHARED_OBJECT_STORE
      assertThat(context.getUpdateGlobalSharedObjectStorePluginState().getRpc())
          .isEqualTo(stateUpdateRpc);
    }
  }

  private VotingState createPollWithPendingUpdate(
      PendingUpdate pendingUpdate, long timeoutTimestamp) {
    BlockchainAddress contract = createContractAddress();
    return VotingState.initial(contract)
        .updateActiveCommitteeVotes(committeeVotes)
        .createPoll(firstHash, firstAddress, "A description", timeoutTimestamp, pendingUpdate);
  }

  private VotingState checkUpdate(VotingState oldState) {
    return serialization.invoke(
        context,
        oldState,
        rpc -> {
          rpc.writeByte(VotingContract.Invocations.CHECK_UPDATE);
          firstHash.write(rpc);
        });
  }

  @Test
  public void invokeReceiveCommitteeInformation() {
    BlockchainAddress bpOrchestrationContract = createContractAddress();
    from(bpOrchestrationContract);
    VotingState updated =
        serialization.invoke(
            context,
            VotingState.initial(bpOrchestrationContract),
            rpc -> {
              rpc.writeByte(VotingContract.Invocations.RECEIVE_COMMITTEE_INFORMATION);
              writeActiveCommitteeVotes(rpc, committeeVotes);
            });
    assertThat(updated.isPartOfCommittee(addressA)).isTrue();
  }

  @Test
  public void invokeReceiveCommitteeInformation_Invalid() {
    BlockchainAddress bpOrchestrationContract = createContractAddress();
    from(bpOrchestrationContract);
    ThrowableAssert.ThrowingCallable committeeInformationNotFromBpOrchestrationContract =
        () ->
            serialization.invoke(
                context,
                VotingState.initial(firstAddress),
                rpc -> {
                  rpc.writeByte(VotingContract.Invocations.RECEIVE_COMMITTEE_INFORMATION);
                  writeActiveCommitteeVotes(rpc, committeeVotes);
                });
    assertThatThrownBy(committeeInformationNotFromBpOrchestrationContract)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the BP orchestration contract can send committee information");
  }

  @Test
  void invokeLocalAccountPlugin() {
    BlockchainAddress blockchainAddress = createContractAddress();
    from(blockchainAddress);
    byte[] accountPluginInvocation =
        SafeDataOutputStream.serialize(
            s -> {
              s.writeByte(2); // ADD_MPC_TOKEN_BALANCE
              s.writeLong(10);
            });
    byte[] invoke =
        SafeDataOutputStream.serialize(
            s -> {
              blockchainAddress.write(s);
              s.writeDynamicBytes(accountPluginInvocation);
            });
    PendingUpdate update =
        PendingUpdate.create(PollUpdateType.INVOKE_LOCAL_ACCOUNT_PLUGIN, invoke, firstHash);
    VotingState pluginUpdateState =
        createPollWithPendingUpdate(update, 0)
            .addVote(firstHash, addressC, true, 0)
            .addVote(firstHash, addressB, true, 0);
    assertThat(pluginUpdateState.getPoll(firstHash).getResult()).isEqualTo(VotingResult.YES);
    VotingState checkUpdateState = checkUpdate(pluginUpdateState);
    assertThat(checkUpdateState.getPoll(firstHash)).isNull();
    LocalPluginStateUpdate pluginState = context.getUpdateLocalAccountPluginStates().get(0);
    assertThat(pluginState.getContext()).isEqualTo(blockchainAddress);
    assertThat(pluginState.getRpc()).isEqualTo(accountPluginInvocation);
  }

  private BlockchainAddress createContractAddress() {
    return BlockchainAddress.fromHash(
        BlockchainAddress.Type.CONTRACT_GOV, Hash.create(s -> s.writeInt(33)));
  }

  private void from(BlockchainAddress fromAddress) {
    context =
        new SysContractContextTest(
            context.getBlockProductionTime(), context.getBlockTime(), fromAddress);
  }

  private void setBlockProductionTime(long blockProductionTime) {
    context =
        new SysContractContextTest(blockProductionTime, context.getBlockTime(), context.getFrom());
  }

  void writeActiveCommitteeVotes(SafeDataOutputStream stream, CommitteeVotes committeeVotes) {
    AvlTree<BlockchainAddress, Long> activeCommitteeVotes = committeeVotes.activeCommitteeVotes();
    stream.writeInt(activeCommitteeVotes.size());
    for (BlockchainAddress address : activeCommitteeVotes.keySet()) {
      address.write(stream);
      stream.writeLong(activeCommitteeVotes.getValue(address));
    }
  }
}
