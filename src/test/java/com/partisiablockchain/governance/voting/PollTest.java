package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.crypto.Hash;
import com.secata.tools.coverage.FunctionUtility;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PollTest {

  private final BlockchainAddress pollOwnerAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");

  private final BlockchainAddress addressA =
      BlockchainAddress.fromString("000000000000000000000000000000000000000003");
  private final BlockchainAddress addressB =
      BlockchainAddress.fromString("000000000000000000000000000000000000000004");
  private final BlockchainAddress addressC =
      BlockchainAddress.fromString("000000000000000000000000000000000000000005");
  private final BlockchainAddress addressD =
      BlockchainAddress.fromString("000000000000000000000000000000000000000006");
  private final BlockchainAddress addressE =
      BlockchainAddress.fromString("000000000000000000000000000000000000000007");
  private final BlockchainAddress addressF =
      BlockchainAddress.fromString("000000000000000000000000000000000000000008");

  private final CommitteeVotes committeeVotes =
      CommitteeVotes.create(
          List.of(
              new CommitteeVote(addressA, 10),
              new CommitteeVote(addressB, 10),
              new CommitteeVote(addressC, 10),
              new CommitteeVote(addressD, 10),
              new CommitteeVote(addressE, 50),
              new CommitteeVote(addressF, 90)));

  private static final Hash identifier = Hash.create(FunctionUtility.noOpConsumer());

  @Test
  public void addVote_MoreThanTwoThirdVotedYesButLessThanHalfWeightedYes() {
    Poll poll =
        createTestPoll()
            .addVote(addressA, true, committeeVotes)
            .addVote(addressB, true, committeeVotes)
            .addVote(addressC, true, committeeVotes)
            .addVote(addressE, false, committeeVotes)
            .addVote(addressD, true, committeeVotes);

    Assertions.assertThat(poll.getResult()).isEqualTo(VotingResult.NO_RESULT);
  }

  @Test
  public void addVote_HasMoreThanOneThirdVotedNo() {
    Poll poll =
        createTestPoll()
            .addVote(addressA, false, committeeVotes)
            .addVote(addressB, false, committeeVotes);

    Assertions.assertThat(poll.getResult()).isEqualTo(VotingResult.NO_RESULT);
  }

  @Test
  public void addVote_CanNoLongerBeWeightedYes() {
    Poll poll = createTestPoll().addVote(addressF, false, committeeVotes);

    Assertions.assertThat(poll.getResult()).isEqualTo(VotingResult.NO);
  }

  @Test
  public void addVote_ExactlyHalfWeightedYes_ShouldNotAloneResultInYes() {
    CommitteeVotes committeeVotes =
        CommitteeVotes.create(
            List.of(
                new CommitteeVote(addressA, 10),
                new CommitteeVote(addressB, 10),
                new CommitteeVote(addressC, 10),
                new CommitteeVote(addressD, 10),
                new CommitteeVote(addressE, 40),
                new CommitteeVote(addressF, 90)));

    Poll poll =
        createTestPoll()
            .addVote(addressE, false, committeeVotes)
            .addVote(addressA, true, committeeVotes)
            .addVote(addressB, true, committeeVotes)
            .addVote(addressC, true, committeeVotes)
            .addVote(addressD, true, committeeVotes);

    Assertions.assertThat(poll.getResult()).isEqualTo(VotingResult.NO_RESULT);
  }

  @Test
  public void hasVoteFromAccount() {
    Poll poll = createTestPoll();
    Assertions.assertThat(poll.hasVoteFromAccount(addressA)).isFalse();

    poll = poll.addVote(addressA, true, committeeVotes);
    Assertions.assertThat(poll.hasVoteFromAccount(addressA)).isTrue();
  }

  @Test
  void timeoutPoll() {
    Hash pollId =
        Hash.fromString("0000000000000000000000000000000000000000000000000000000000000001");
    VotingState votingstate = VotingState.initial(pollOwnerAddress);
    SysContractContextTest context = new SysContractContextTest(99, 1, pollOwnerAddress);

    VotingState updatedVotingState =
        votingstate.createPoll(
            pollId,
            addressA,
            "Test poll",
            98,
            PendingUpdate.create(PollUpdateType.ENABLE_FEATURE, new byte[0], identifier));

    updatedVotingState = updatedVotingState.checkUpdate(pollId, context);

    Assertions.assertThat(updatedVotingState.getPolls().containsKey(pollId)).isFalse();
  }

  private Poll createTestPoll() {
    return Poll.initial(
        pollOwnerAddress,
        "Description",
        1,
        PendingUpdate.create(PollUpdateType.ENABLE_FEATURE, new byte[0], identifier));
  }
}
