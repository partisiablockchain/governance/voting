package com.partisiablockchain.governance.voting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AccountPluginRpcTest {

  @Test
  void getStakedTokens() {
    byte[] rpc = AccountPluginRpc.getStakedTokens();
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.GET_STAKED_TOKENS);
  }

  @Test
  void proposeUpdateCallback() {
    Hash pollId =
        Hash.fromString("0000000000000000000000000000000000000000000000000000000000000001");
    BlockchainAddress owner =
        BlockchainAddress.fromString("000000000000000000000000000000000000000005");
    String description = "Very informative";
    PendingUpdate pendingUpdate =
        PendingUpdate.create(
            PollUpdateType.UPDATE_PLUGIN, new byte[0], Hash.create(FunctionUtility.noOpConsumer()));
    byte[] rpc =
        SafeDataOutputStream.serialize(
            VotingContract.Callbacks.proposeUpdate(pollId, owner, description, pendingUpdate));
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(0);
    assertThat(Hash.read(stream)).isEqualTo(pollId);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(owner);
    assertThat(stream.readString()).isEqualTo(description);
    assertThat(readPendingUpdate(stream)).usingRecursiveComparison().isEqualTo(pendingUpdate);
  }

  private static PendingUpdate readPendingUpdate(SafeDataInputStream rpc) {
    PollUpdateType pollUpdateType = rpc.readEnum(PollUpdateType.values());
    byte[] updateRpc = rpc.readDynamicBytes();
    Hash identifier = Hash.read(rpc);
    return new PendingUpdate(pollUpdateType, new LargeByteArray(updateRpc), identifier);
  }
}
